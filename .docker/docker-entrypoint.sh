#!/bin/sh

ROOT_FOLDER="/var/www/html"
MOODLE_FOLDER="$ROOT_FOLDER/moodle"
DATA_FOLDER="$ROOT_FOLDER/moodledata"

set -e

 if [ ! -f "$MOODLE_FOLDER/config.php" ]; then
   echo "=================================================="
   echo "Please wait. Preparing moodle installation."
   echo "=================================================="

   if [ ! -f "$ROOT_FOLDER/v$MOODLE_VERSION.tar.gz" ]; then
     echo "Moodle not found! Downloading from https://github.com/moodle/moodle/archive/v$MOODLE_VERSION.tar.gz"
     wget --no-verbose https://github.com/moodle/moodle/archive/v$MOODLE_VERSION.tar.gz -P $ROOT_FOLDER
   fi

   tar xfz $ROOT_FOLDER/v$MOODLE_VERSION.tar.gz -C $ROOT_FOLDER && cp -r $ROOT_FOLDER/moodle-$MOODLE_VERSION/* $MOODLE_FOLDER

 echo "=================================================="

  echo "Installing moodle... this can take a while."

  echo "=================================================="

  cd $MOODLE_FOLDER/admin/cli

  php install.php --wwwroot="http://127.0.0.1:9000" --dataroot="$DATA_FOLDER" --dbhost="db" --dbtype="mysqli" --dbname="laravel" --dbport="3306" --dbuser="root" --dbpass="rootpass" --fullname="Moodle LMS" --shortname="Moodle" --adminuser="admin" --adminpass="adminmoodle" --agree-license --non-interactive

  if [ $? -eq 0 ]; then

    echo "=================================================="

    chown -R www-data:www-data $MOODLE_FOLDER $DATA_FOLDER

    echo "INSTALLATION COMPLETE!"

    echo

    echo "url: http://$MOODLE_HOSTNAME"

    echo

    echo "admin user: $MOODLE_USER"

    echo "admin pass: $MOODLE_PASS"

    echo "================================================"

  else

    echo "=================================================="

    echo " INSTALLATION ABORTED! MOODLE IS NOT INSTALLED. "

    echo "=================================================="

  fi
 fi

exec "$@"

# exec supervisord -n -c /etc/supervisord.conf
